//const express = require('express)const app = express()app.listen(8080, () => {  console.log("Serveur prêt")}')
var express = require("express");
var app = express();
var router = express.Router();

//init connection redis db
// const redis = require('redis');
// const client = redis.createClient({
//   host: 'localhost',
//   port: 6379
// });

// client.on('connect', function() {
//     console.log('Connected!');
//   });

var path = __dirname + '/views/';

const PORT = 8080;
const HOST = '0.0.0.0';

router.use(function (req,res,next) {
    console.log("/" + req.method);
    next();
  });

// router.get("/",function(req,res){
//     res.sendFile(path + "index.html");
// });

router.get("/api",function(req,res){
  res.sendFile(path + "plats.html");
});
  
router.get("/sharks",function(req,res){
    res.sendFile(path + "sharks.html");
});
  
app.use(express.static(path));
app.use("/", router);

app.listen(8090, function(){
    console.log('server is up !')
})

// https://www.cloudamqp.com/blog/how-to-run-rabbitmq-with-nodejs.html to connect with rabbitMQL