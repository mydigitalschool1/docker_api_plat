FROM node:10-alpine

RUN mkdir -p /home/node/docker_api_plat/node_modules && chown -R node:node /home/node/docker_api_plat

WORKDIR /home/node/docker_api_plat

COPY package*.json ./

USER node

RUN npm ci

COPY --chown=node:node . .

EXPOSE 8080

CMD [ "node", "index.js" ]